import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    public static void main(String[] args) {
        List<Book> BookList = new ArrayList<>();
        int id = 0;
        while (true){
            Scanner scan = new Scanner(System.in);
            System.out.println("1-Add\n2-Search\n3-Delete");
            int j = scan.nextInt();
            if (j > 3 || j < 0)
                break;
            switch (j){
                case 1:
                    BookList.add(new Book(id, Book.Inputer("Имя"), Book.Inputer("Автор"), Integer.valueOf(Book.Inputer("Год издания"))));
                    id++;
                    break;
                case 2:
                if (BookList.size() == 0) {
                    System.out.println(BookList.size());
                    System.out.println("У вас нет книг");
                    continue;
                }
                else {
                    System.out.println("1-Имя\n2-Автор\n3-Год издания\n");
                    int v = scan.nextInt();
                    if (v > 3 || v < 0)
                        break;
                    switch (v){
                        case 1:
                            System.out.print("Введите имя книги: ");
                            String name = scan.next();
                            for (int i = 0; i < BookList.size(); i++) {
                                if (BookList.get(i).Name.equals(name))
                                    System.out.println("ID: " + (BookList.get(i).ID) + " Автор: " + BookList.get(i).Author + " Название: " + BookList.get(i).Name + " Год издания " + BookList.get(i).Year);
                            }
                            System.out.println();
                            break;
                        case 2:
                            System.out.print("Введите автора книги: ");
                            String author = scan.next();
                            for (int i = 0; i < BookList.size(); i++) {
                                if (BookList.get(i).Author.equals(author))
                                    System.out.println("ID: " + (BookList.get(i).ID) + " Автор: " + BookList.get(i).Author + " Название: " + BookList.get(i).Name + " Год издания " + BookList.get(i).Year);
                            }
                            System.out.println();
                            break;
                        case 3:
                            System.out.print("Введите год книги: ");
                            int year = scan.nextInt();
                            for (int i = 0; i < BookList.size(); i++) {
                                if (BookList.get(i).Year == year)
                                    System.out.println("ID: " + (BookList.get(i).ID) + " Автор: " + BookList.get(i).Author +  " Название: " + BookList.get(i).Name + " Год издания " + BookList.get(i).Year);
                            }
                            System.out.println();
                            break;
                    }
                    break;
                }
                case 3:
                    if (BookList.size() == 0) {
                        System.out.println(BookList.size());
                        System.out.println("У вас нет книг");
                        continue;
                    }
                    else{
                    System.out.println("Введите номер удаляемой книги");
                        int v = scan.nextInt();
                        if (v > BookList.size() || v < 0)
                            break;
                        BookList.remove(v);
                    }
                    break;
            }
    }
    }
}
