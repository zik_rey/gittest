import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2){
        Double APoint1 = new Double(student1.getAPoint());
        Double APoint2 = new Double(student2.getAPoint());
        return APoint1.compareTo(APoint2);
    }
}
