import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Student> student = new ArrayList<>();
        student.add(new Student("Николай Владимирович",1,5,5,4,3,4));
        student.add(new Student("Василий Еремеевич",1,2,3,4,3,3));
        student.add(new Student("Пётр Глебович",1,5,5,4,2,2));
        student.add(new Student("Жеберн Пантилимонович",2,5,5,5,5,5));
        student.add(new Student("Александр Сергеевич",2,2,3,3,3,4));
        student.add(new Student("Павел Константинович",2,3,5,3,4,4));
        student.add(new Student("Борис Михайлович",3,4,5,4,4,4));
        student.add(new Student("Валентин Грегорьевич",3,3,2,4,3,3));
        student.add(new Student("Евгений Юрьевич",3,4,4,3,2,2));
        student.add(new Student("Денис Андреевич",3,4,3,2,3,5));


        while(true) {
            System.out.println("1-View students' group 2-View students' APoint 3-View the best students");
            Scanner scaner = new Scanner(System.in);
            int v = scaner.nextInt();
            switch (v){
                case 1:
                    for(int i = 0; i < 10; i++)
                        System.out.println(student.get(i).Name + "Группа " + student.get(i).GroupNum);
                    break;
                case 2:
                    student.sort(new StudentComparator());
                    Collections.reverse(student);
                    for(int i = 0; i < 10; i++)
                        System.out.println(student.get(i).Name + "Средний балл" + student.get(i).APoint);
                        break;
                case 3:
                    for(int i = 0; i < 10; i++)
                        if(student.get(i).APoint>=4)
                        System.out.println(student.get(i).Name + "Группа " + student.get(i).APoint);
                    break;
            }
        }
    }
}
