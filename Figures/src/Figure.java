import java.util.*;

abstract public class Figure {
    abstract public double Perimeter();
    abstract  public double Area();

    public static String Inputer(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

    public static void FigureCheck(List<Figure> a){
        int i = 0;
        for (Figure pr : a) {
            if (pr instanceof Triangle) {
                System.out.println("ID:" + (i) + " Сторона 1: " + ((Triangle) pr).getK1() + " Сторона 2: " + ((Triangle) pr).getK2() +  " Сторона 3: " + ((Triangle) pr).getG() + " Угол: "+ ((Triangle) pr).getAngle()+ " Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            if (pr instanceof Rectangle) {
                System.out.println("ID:" + (i) + " Сторона 1: " + ((Rectangle) pr).getA() + "Сторона 2: " + ((Rectangle) pr).getB() +" Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            if (pr instanceof Circle) {
                System.out.println("ID:" + (i) + " Радиус: " + ((Circle) pr).getR() +" Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            i++;
        }
    }
}
