public class Triangle extends  Figure {
    private double k1;
    private double k2;
    private double g;
    private double angle;
    public Triangle(double k1, double k2, double angle){
        this.k1 = k1;
        this.k2 = k2;
        this.angle = angle;
        this.g = Math.sqrt((k1*k1)+(k2*k2)-2*k1*k2*Math.cos(angle));
    }

    public double getAngle() {
        return angle;
    }

    public double getG() {
        return g;
    }

    public double getK1() {
        return k1;
    }

    public double getK2() {
        return k2;
    }

    @Override
    public double Perimeter() {
        return k1+k2+g;
    }

    @Override
    public double Area() {
        double p = (k1+k2+g)/2;
        double h =(2*Math.sqrt(p*(p-g)*(p-k1)*(p-k2)))/g;
        return (g/2)*h;
    }
}
